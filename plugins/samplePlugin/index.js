/**
 * This is the example module
 * @type {{init: module.exports."init", provides: module.exports."provides", hooks: module.exports."hooks", config: module.exports."config"}}
 */
var path = require('path');

module.exports = {

    "provides":function(){

    },

    "hooks":function(){


    },

    "helpers":function(){



    },

    "onLoad":function(){


    },

    "config":function(){


    }

};