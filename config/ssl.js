/**
 * SSL certificate
 * */

module.exports = {
    "env": "dev",

    "dev": {

        "certificates": [{
            "name": "your_certificate_name",
            "key": "",
            "cert": "",
        }]

    },

};