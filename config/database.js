/**
 * Database configuration
 * */

module.exports = {
    //Environment can be dev or prod
    "env": "dev",

    "dev": {
        "url": "",

        "host": process.env.DB_HOST || "127.0.0.1",

        "port":process.env.DB_PORT || 3306,

        "name": "runnerpixidb",

        "username": process.env.DB_USER || 'root',

        "password": process.env.DB_PASSWORD || '',

        "dialect": process.env.DB_DIALECT || "mysql",

        "sql_mode":"" || process.env.DB_SQL_MODE,

        "logging":false,

        "store":"",

        "force_sync":false

    },

    "prod": {
        "host": process.env.DB_HOST || "127.0.0.1",

        "port":process.env.DB_PORT || 3306,

        "name": process.env.DB_NAME || "shinjs",

        "username": process.env.DB_USER || 'root',

        "password": process.env.DB_PASSWORD || '',

        "dialect": process.env.DB_DIALECT || "mysql",

        "sql_mode":"" || process.env.DB_SQL_MODE,

        "logging":false,

        "store":"",

        "force_sync":false

    }


};