/**
 * Global variables configuration
 * */

module.exports = {

    "env": "dev",

    "dev": {

        "modules": true,

        "models": true,

        "plugins": true,

        "hooks": true,

        "library": true,

        "shinjs": true,

        "io": true

    }


};