/**
 * Configuration for Assets module
 * */

module.exports = {
    "env": "dev",
    "dev": {

        "bower_url": "",

        "assets_url": "/components",

        "overrides": { //Overrides packages bower.json. This is used when a package is missing a main config
            "bootstrap": {
                "main": [
                    "dist/css/bootstrap.min.css",
                    "dist/js/bootstrap.min.js"
                ]
            }
        },

        "themes": [{
            name: 'default', // Set name of the theme

            "includes":{ // Assets to include to this theme. Set your own custom css and js files

                "css" : [
                    "/components/AdminLTE/dist/css/skins/_all-skins.css",
                    "/components/iCheck/skins/square/blue.css",
                    "/public/css/main.css"
                ],

                "js" : [
                    "/public/js/main.js",
                    "/public/js/vendor/pixi.js","/public/js/game.js","/public/js/runner.js"
                ]
            },

            excludes: [
                "/bower_components/datatables/media/css/jquery.dataTables.css",
                "/bower_components/datatables/media/js/jquery.dataTables.js"
            ] // Array of bower packages to exclude from the theme.

        },{
            name: 'game', // Set name of the theme

            "includes":{ // Assets to include to this theme. Set your own custom css and js files

                "css" : ["/public/css/game.css"],

                "js" : ["/public/js/vendor/pixi.js","/public/js/game.js","/public/js/runner.js"]
            },

            excludes: ['/AdminLTE/'] // Array of bower packages to exclude from the theme.

        }]



    }
};