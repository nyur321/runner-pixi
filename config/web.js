/**
 * App Configuration
 * */

module.exports = {
    "env": "dev",
    "dev": {
        "title": "Shift App",

        "host":null,

        "port": "1337",

        "proxy": null,

        "static_dir": [
            "public"
        ],

        "timezone": "Asia/Manila",

        "session":"runnergame_session",

        "session_secret":"abc123456",

        "session_store":"database", //memory or database

        "log_level": "info", // error,warn,info,verbose,debug,silly

        "uploads":"/uploads"
    },


    "prod": {
        "title": "Shift App",
        "static_dir": [
            "public"
        ],

        "host":"localhost",

        "port": "3000",

        "proxy": null,

        "timezone": "Asia/Manila",

        "session":"shift_session_default",

        "session_secret":"abc123456",

        "views":{
            "engine":"ejs-mate"
        }
    }
};