"use strict";

module.exports = {
    update:function (req,res,user) {
        req.session.user = {
            id:user.id,
            role_id:user.role_id,
            email:user.email,
            first_name:user.person.first_name,
            last_name:user.person.last_name,
        };
        res.locals.user = {
            id:user.id,
            role_id:user.role_id,
            email:user.email,
            fname:user.person.first_name,
            lname:user.person.last_name,
        };

        return true;
    },
    reset:function(req,res){
        res.locals.user = {
            id:0,
            fname:'Anonymous',
            lname:'Account',
        };

        return true;

    },
    destroy:function (req,res) {
        res.locals.user = {
            id:0,
            fname:'Anonymous',
            lname:'Account',
        };
        req.session.destroy();
        req.session = null;

    }
    /** Add your own helper functions here */


};