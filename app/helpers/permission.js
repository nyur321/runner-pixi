"use strict";

module.exports = {
    access: function (req, res, access, ajax) {
        var role_id = (req.session.hasOwnProperty('user') ? req.session.user.role_id : res.locals.user.role_id);
        if (Array.isArray(access)) {
            if (access.indexOf(role_id) != -1) {
                return true;
            } else {
                if (ajax)
                    res.status(403).send({error: 1, message: 'Not permitted'});
                else
                    res.status(403).render('_403', {message: 'Sorry You\'re not permitted to access this page.'});

                return false;
            }


        } else {
            return true;
        }


    }
    /** Add your own helper functions here */


};