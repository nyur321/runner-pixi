"use strict"

module.exports = function (sequelize, DataTypes) {
    var Questions = sequelize.define("questions", {
        question: DataTypes.STRING,
        answer: DataTypes.STRING,
        choices: DataTypes.TEXT,
        display_order: {
            type: DataTypes.INTEGER
        }
    }, {
        updatedAt: 'updated_at',
        createdAt: 'created_at',
        logging: function (log) {
            //console.log(log);
        },
        classMethods: {
            associate: function (models) {
                Questions.hasMany(models.answers,{foreignKey:'question_id'});
            }
        }
    });



    return Questions;
};