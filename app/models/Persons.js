"use strict"

module.exports = function (sequelize, DataTypes) {
    var Persons = sequelize.define("persons", {
        first_name: DataTypes.STRING,
        last_name: DataTypes.STRING,
    }, {
        updatedAt: 'updated_at',
        createdAt: 'created_at',
        logging: function (log) {
            //console.log(log);
        },
        classMethods: {
            associate: function (models) {
                Persons.hasOne(models.users,{foreignKey:'person_id'});
                Persons.hasMany(models.questionnaire,{foreignKey:'author'});
            }
        }
    });



    return Persons;
};