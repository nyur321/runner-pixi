"use strict"

module.exports = function (sequelize, DataTypes) {
    var Roles = sequelize.define("roles", {
        role: DataTypes.STRING,
        description: DataTypes.STRING
    }, {
        updatedAt: 'updated_at',
        createdAt: 'created_at',
        logging: function (log) {
            //console.log(log);
        },
        classMethods: {
            associate: function (models) {
                Roles.hasOne(models.users,{foreignKey:'role_id'});
            }
        }
    });



    return Roles;
};