"use strict"

module.exports = function (sequelize, DataTypes) {
    var Questionnaire = sequelize.define("questionnaire", {
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        author: DataTypes.INTEGER,
        type: DataTypes.STRING,
        length:DataTypes.INTEGER
    }, {
        updatedAt: 'updated_at',
        createdAt: 'created_at',
        logging: function (log) {
            //console.log(log);
        },
        classMethods: {
            associate: function (models) {
                Questionnaire.hasMany(models.questions,{foreignKey:'questionnaire_id'});
                Questionnaire.belongsTo(models.users,{foreignKey:'author'});
            },
            create:function (questionnaire,callback) {
                Questionnaire.build(questionnaire).save().then(function (result) {
                    callback(null,result);
                });
            }
        }
    });



    return Questionnaire;
};