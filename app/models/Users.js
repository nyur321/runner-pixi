"use strict"

module.exports = function (sequelize, DataTypes) {
    var Users = sequelize.define("users", {
        username: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        role_id:DataTypes.INTEGER,
        is_deleted:{
            type: DataTypes.INTEGER,
            defaultValue: 0,
        },
        remember_token: DataTypes.STRING,
        validated: DataTypes.INTEGER
    }, {
        updatedAt: 'updated_at',
        createdAt: 'created_at',
        logging: function (log) {
            //console.log(log);
        },
        classMethods: {
            associate: function (models) {
                Users.belongsTo(models.persons,{foreignKey:'person_id'});
                Users.belongsTo(models.roles,{foreignKey:'role_id'});
                Users.hasMany(models.answers,{foreignKey:'user_id'});
                Users.hasMany(models.questionnaire,{foreignKey:'author'});

            },
            login:function (user,callback) {
                Users.findOne({
                    where:{
                        email:user['email'],
                        password:user['password']
                    }
                }).then(function (user) {
                    if(user){
                        callback(null,user)
                    }else{
                        callback('User not found', null);
                    }

                })
            },
            
            register:function (user,callback) {
                Users.findOne({
                    where:{
                        email:user['email']
                    }
                }).then(function (user_res) {
                    if(user_res){
                        callback('Email '+user.email+' already taken', null);
                    }else{
                        sequelize.models.persons.create({
                            first_name:user.first_name,
                            last_name:user.last_name,
                        }).then(function (person) {

                            Users.create({
                                email:user.email,
                                username:user.username,
                                password:user.password,
                                role_id:user.hasOwnProperty('role_id') ? user.role_id : 3,
                                person_id:person.get({plain:true}).id
                            }).then(function (result) {
                                callback(null,result);
                            })
                        })



                    }
                })
            }
            
            
        }
    });



    return Users;
};