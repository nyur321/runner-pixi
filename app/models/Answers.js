"use strict"

module.exports = function (sequelize, DataTypes) {
    var Answers = sequelize.define("answers", {
        answer: DataTypes.STRING,
        user_id: DataTypes.INTEGER,
        question_id: DataTypes.INTEGER,
    }, {
        updatedAt: 'updated_at',
        createdAt: 'created_at',
        logging: function (log) {
            //console.log(log);
        },
        classMethods: {
            associate: function (models) {
                Answers.belongsTo(models.questions,{foreignKey:'question_id'});
                Answers.belongsTo(models.users,{foreignKey:'user_id'});
            }
        }
    });



    return Answers;
};