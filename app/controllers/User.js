module.exports = {

    "index": ['GET', function (req, res) {

        res.register('user/profile');

    }, {title: 'My Home Page', name: 'home'}],
    
    "manage":['GET',function (req,res) {
        var permitted = $.helpers.permission.access(req,res,[1,2],false);

        if(permitted){

            res.render('user');

        }


    }],

    "create":['GET',function (req,res) {
        var permitted = $.helpers.permission.access(req,res,[1,2],false);

        if(permitted){

            res.render('user/create');

        }


    }],

    "profile":['GET',function (req,res) {
        var permitted = $.helpers.permission.access(req,res,[1,2,3],false);

        if(permitted){
            $.models.users.findOne({
                where:{
                    id:req.session.user.id
                },
                include:[$.models.persons]
            }).then(function (user) {
                res.render('user/profile',{data:user});
            });

        }


    }],

    "logout":['GET',function (req,res) {
        $.helpers.session.destroy(req,res);

        res.render('main');


    }]


};