"use strict";


var async = require('async');
var datatable = require('sequelize-datatables');
var _ = require('lodash');

module.exports = {

    "index": ['GET', function (req, res) {

        res.render('main');

    }, {title: 'api', name: 'api'}],


    "register": ['POST', function (req, res) {

        req.checkBody({
            'email': {
                isEmail: {
                    errorMessage: 'Invalid Email'
                },
                errorMessage: 'Email is required'
            },
            'username': {
                notEmpty: true,
                errorMessage: 'Username is required'
            },
            'first-name': {
                notEmpty: true,
                isLength: {
                    options: [{min: 2, max: 50}],
                    errorMessage: 'Must be between 2 and 50 chars long' // Error message for the validator, takes precedent over parameter message
                },
                errorMessage: 'First name is required' // Error message for the parameter
            },
            'last-name': {
                isLength: {
                    options: [{min: 2, max: 50}],
                    errorMessage: 'Must be between 2 and 50 chars long' // Error message for the validator, takes precedent over parameter message
                },
                errorMessage: 'Invalid First Name'
            },
            'password': {
                isLength: {
                    options: [{min: 5, max: 32}],
                    errorMessage: 'Must be between 2 and 32 chars long' // Error message for the validator, takes precedent over parameter message
                },
                errorMessage: 'Password is required'
            }
        });


        req.asyncValidationErrors().then(function () {
            $.models.users.register({
                email: req.body['email'],
                username: req.body['username'],
                first_name: req.body['first-name'],
                last_name: req.body['last-name'],
                password: req.body['password']
            }, function (err, user) {
                if (err) {
                    res.send({
                        error: 1,
                        message: 'Error registration of user',
                        data: {
                            errors: [
                                {
                                    value: req.body.email,
                                    msg: err,
                                    param: 'email'
                                }
                            ]
                        }
                    });
                } else {

                    $.models.users.findOne({
                        where: {
                            id: user.id
                        },
                        include: [{model: $.models.persons}]
                    }).then(function (findres) {
                        $.helpers.session.update(req, res, findres);
                        res.send({error: 0, message: 'User registered', data: {user: findres}});
                    });


                }
            });
        }, function (errors) {
            res.send({error: 1, message: 'Please check fields', data: {errors: errors}});


        });


    }, {name: 'register'}],


    "update_profile": ['POST', function (req, res) {
        var permitted = $.helpers.permission.access(req, res, [1, 2, 3], true);
        if (permitted) {
            var validate_fields = {
                'email': {
                    isEmail: {
                        errorMessage: 'Invalid Email'
                    },
                    errorMessage: 'Email is required'
                },
                'username': {
                    notEmpty: true,
                    errorMessage: 'Username is required'
                },
                'first-name': {
                    notEmpty: true,
                    isLength: {
                        options: [{min: 2, max: 50}],
                        errorMessage: 'Must be between 2 and 50 chars long' // Error message for the validator, takes precedent over parameter message
                    },
                    errorMessage: 'First name is required' // Error message for the parameter
                },
                'last-name': {
                    isLength: {
                        options: [{min: 2, max: 50}],
                        errorMessage: 'Must be between 2 and 50 chars long' // Error message for the validator, takes precedent over parameter message
                    },
                    errorMessage: 'Invalid First Name'
                },
                'change-password': {
                    optional: {
                        options: {checkFalsy: true}, // or: [{ checkFalsy: true }]
                        errorMessage: 'Change password'
                    }
                },
                'current-password': {
                    notEmpty: true,
                    isLength: {
                        options: [{min: 5, max: 32}],
                        errorMessage: 'Must be between 2 and 32 chars long' // Error message for the validator, takes precedent over parameter message
                    },
                    errorMessage: 'You must type your current password'
                }
            };

            if (req.body.hasOwnProperty('change-password')) {
                if (req.body['change-password'] != '') {
                    validate_fields['repeat-password'] = {
                        matches: {
                            options: [req.body['change-password'], 'i'], // pass options to the validator with the options property as an array
                            // options: [/example/i] // matches also accepts the full expression in the first parameter
                            errorMessage: 'Passwords don\'t match'
                        },
                        errorMessage: 'Password is required'
                    };
                }

            }


            req.checkBody(validate_fields);

            req.asyncValidationErrors().then(function () {

                $.models.users.findOne({
                    where: {
                        id: req.session.user.id,
                        password: req.body['current-password']
                    }
                }).then(function (user) {
                    if (user) {
                        var return_user = {};
                        async.series([function (callback) {
                            user.update({
                                email: req.body['email'],
                                username: req.body['username']
                            }).then(function (updated_user) {
                                if (updated_user) {
                                    return_user = {
                                        id: updated_user.id,
                                        email: updated_user.email,
                                        username: updated_user.username,
                                        person_id: updated_user.person_id,
                                    };
                                    callback();

                                } else {
                                    callback({error: 'user not updated'});

                                }
                            });
                        }, function (callback) {
                            $.models.persons.findOne({
                                where: {
                                    id: return_user.person_id
                                }
                            }).then(function (person) {
                                person.update({
                                    first_name: req.body['first-name'],
                                    last_name: req.body['last-name'],
                                }).then(function (updated_person) {
                                    return_user['first_name'] = updated_person.first_name;
                                    return_user['last_name'] = updated_person.last_name;
                                    callback()
                                })


                            });

                        }], function (err) {
                            if (!err) {
                                res.send({error: 0, message: 'Profile updated', data: {user: return_user}});
                            } else {
                                res.send({error: 1, message: 'Profile not updated. Server error', data: {errors: []}});
                            }


                        })

                    } else {
                        res.send({
                            error: 1, message: 'Current password is incorrect', data: {
                                errors: [{
                                    msg: 'Current password is incorrect',
                                    param: 'current-password',
                                    value: req.body['current-password']
                                }]
                            }
                        });
                    }

                });


            }, function (errors) {

                res.send({error: 1, message: 'Please check fields', data: {errors: errors}});

            });
        }
    }],


    "login": ['POST', function (req, res) {
        req.checkBody({
            'login-email': {
                notEmpty: true,
                isEmail: {
                    errorMessage: 'Invalid Email'
                },
                errorMessage: 'Email is required'
            },
            'login-password': {
                notEmpty: true,
                errorMessage: 'Please provide password'
            },
        });


        req.asyncValidationErrors().then(function () {
            $.models.users.login({
                email: req.body['login-email'],
                password: req.body['login-password']
            }, function (err, user) {
                if (err) {
                    res.send({
                        error: 1, message: 'Error registration of user', data: {
                            errors: [
                                {
                                    msg: '',
                                    param: 'login-email',
                                    value: req.body['login-email'],
                                },
                                {
                                    msg: 'Invalid username or password',
                                    param: 'login-password',
                                    value: req.body['login-password'],
                                },
                            ]
                        }
                    });
                } else {
                    if(user.is_deleted == 1){
                        res.send({error: 1, message: 'User has been removed or disabled',code:'user_deleted'});
                    }else{
                        $.models.users.findOne({
                            where: {
                                id: user.id
                            },
                            include: [{model: $.models.persons}]
                        }).then(function (findres) {
                            $.helpers.session.update(req, res, findres);
                            res.send({error: 0, message: 'User registered', data: {user: {username: findres.username}}});
                        });
                    }



                }
            });
        }, function (errors) {
            res.send({error: 1, message: 'Please check fields', data: {errors: errors}});


        });
    }, {name: 'login'}],


    "create_questionnaire": ['POST', function (req, res) {
        var permitted = $.helpers.permission.access(req, res, [1, 2], false);
        if (permitted) {
            req.checkBody({
                'questionnaire-title': {
                    notEmpty: true,
                    errorMessage: 'Questionnaire title is required'
                },
                'questionnaire-description': {
                    notEmpty: true,
                    isLength: {
                        options: [{min: 5, max: 100}],
                        errorMessage: 'Must be between 5 and 100 chars long' // Error message for the validator, takes precedent over parameter message
                    },
                    errorMessage: 'Description is required'
                }
            });

            req.asyncValidationErrors().then(function () {
                $.models.questionnaire.create({
                    title: req.body['questionnaire-title'],
                    description: req.body['questionnaire-description'],
                    author: req.session.user.id,
                    type: 'normal'
                }, function (err, questionnaire) {
                    // console.log(questionnaire);
                    res.send({
                        error: 0,
                        message: 'Questionnaire created',
                        data: {id: questionnaire.id, redirect: '/questionnaire/edit?id=' + questionnaire.id}
                    });
                });


            }, function (errors) {

                res.send({error: 1, message: 'Please check fields', data: {errors: errors}});


            });


        }


    }, {name: 'create_questionnaire'}],


    "create_user": ['POST', function (req, res) {
        var permitted = $.helpers.permission.access(req, res, [1, 2], false);
        if (permitted) {
            req.checkBody({
                'email': {
                    isEmail: {
                        errorMessage: 'Invalid Email'
                    },
                    errorMessage: 'Email is required'
                },
                'username': {
                    notEmpty: true,
                    errorMessage: 'Username is required'
                },
                'first-name': {
                    notEmpty: true,
                    isLength: {
                        options: [{min: 2, max: 50}],
                        errorMessage: 'Must be between 2 and 50 chars long' // Error message for the validator, takes precedent over parameter message
                    },
                    errorMessage: 'First name is required' // Error message for the parameter
                },
                'last-name': {
                    isLength: {
                        options: [{min: 2, max: 50}],
                        errorMessage: 'Must be between 2 and 50 chars long' // Error message for the validator, takes precedent over parameter message
                    },
                    errorMessage: 'Invalid First Name'
                },
                'password': {
                    isLength: {
                        options: [{min: 5, max: 32}],
                        errorMessage: 'Must be between 2 and 32 chars long' // Error message for the validator, takes precedent over parameter message
                    },
                    errorMessage: 'Password is required'
                }
            });

            var role_id_body = req.body.hasOwnProperty('role-id') ? req.body['role-id'] : 3;

            req.asyncValidationErrors().then(function () {
                $.models.users.register({
                    email: req.body['email'],
                    username: req.body['username'],
                    first_name: req.body['first-name'],
                    last_name: req.body['last-name'],
                    password: req.body['password'],
                    role_id: parseInt(role_id_body, 10)
                }, function (err, user) {
                    if (err) {
                        res.send({
                            error: 1,
                            message: 'Error registration of user',
                            data: {
                                errors: [
                                    {
                                        value: req.body.email,
                                        msg: err,
                                        param: 'email'
                                    }
                                ]
                            }
                        });
                    } else {

                        $.models.users.findOne({
                            where: {
                                id: user.id
                            },
                            include: [{model: $.models.persons}]
                        }).then(function (findres) {
                            res.send({error: 0, message: 'User registered', data: {user: findres}});
                        });


                    }
                });
            }, function (errors) {
                res.send({error: 1, message: 'Please check fields', data: {errors: errors}});


            });


        }


    }, {name: 'create_questionnaire'}],

    "update_user": ['POST', function (req, res) {
        var permitted = $.helpers.permission.access(req, res, [1, 2], false);
        if (permitted) {
            req.checkBody({
                'email': {
                    isEmail: {
                        errorMessage: 'Invalid Email'
                    },
                    errorMessage: 'Email is required'
                },
                'username': {
                    notEmpty: true,
                    errorMessage: 'Username is required'
                },
                'first-name': {
                    notEmpty: true,
                    isLength: {
                        options: [{min: 2, max: 50}],
                        errorMessage: 'Must be between 2 and 50 chars long' // Error message for the validator, takes precedent over parameter message
                    },
                    errorMessage: 'First name is required' // Error message for the parameter
                },
                'last-name': {
                    isLength: {
                        options: [{min: 2, max: 50}],
                        errorMessage: 'Must be between 2 and 50 chars long' // Error message for the validator, takes precedent over parameter message
                    },
                    errorMessage: 'Invalid First Name'
                }
            });

            var role_id_body = req.body.hasOwnProperty('role-id') ? req.body['role-id'] : 3;
            var user_data, update_obj;
            req.asyncValidationErrors().then(function () {
                $.models.users.findOne({
                    where: parseInt(req.body['update-id'], 10),
                    include: [{all: true}]
                }).then(function (user) {
                    user_data = user;
                    update_obj = {
                        username: req.body.username,
                        email: req.body.email,
                        role_id: parseInt(role_id_body, 10)
                    };
                    if (req.body['password'].length >= 5) {
                        update_obj = req.body['password'];
                    }
                    return user.update(update_obj);

                }).then(function (user) {
                    update_obj['first_name'] = req.body['first-name'];
                    update_obj['last_name'] = req.body['last-name'];

                    return user_data.person.update({
                        first_name: req.body['first-name'],
                        last_name: req.body['last-name'],
                    });


                }).then(function (user) {
                    if (user) {
                        res.send({error: 0, message: 'User updated', data: {user: update_obj}});
                    } else {
                        res.send({error: 1, message: 'Could not find user', data: {user: null}});
                    }
                })
            }, function (errors) {
                res.send({error: 1, message: 'Please check fields', data: {errors: errors}});


            });


        }


    }, {name: 'create_questionnaire'}],


    "get_questionnaires": ['POST', function (req, res) {


        datatable($.models.questionnaire, req.body, {
            // attributes:['id','title','description'],
            include: [{all: true}]
        }).then(function (result) {
            res.send(result);
        });


    }, {name: 'get_questionnaires'}],


    "get_users": ['POST', function (req, res) {
        datatable($.models.users, req.body, {
            where: {
                $and:{
                    id: {
                        $not: 1
                    },
                    is_deleted:{
                        $not:1
                    }
                }

            },
            // attributes:['id','title','description'],
            include: [{all: true}]
        }).then(function (result) {
            res.send(result);
        });
    }, {name: 'get_users'}],


    "get_user": ['POST', function (req, res) {
        $.models.users.findOne({
            where: {
                $and:{id: req.body.id,$not:{
                    is_deleted:1
                }}
            },
            include: [{all: true}]
        }).then(function (user) {
            res.send(user);
        })
    }, {name: 'get_users'}],

    "delete_user": ['POST', function (req, res) {
        $.models.users.findOne({
            where: {id: req.body.id},
            include: [{all: true}]
        }).then(function (user) {
            return user.update({
                is_deleted:1
            })

        }).then(function () {
            res.send({error: 0, message: 'success delete', data: {user: req.body['id']}})
        })
    }, {name: 'get_users'}],


    "add_question": ['POST', function (req, res) {
        var permitted = $.helpers.permission.access(req, res, [1, 2], true);


        if (permitted) {
            var choices = req.body['choices'];
            var key_answer = _.findKey(choices, function (o) {
                return o.answer == 'true';
            });
            var max_display_order = 1;
            req.checkBody({
                'question': {
                    notEmpty: true,
                    errorMessage: 'This field is required'
                }

            });

            req.asyncValidationErrors().then(function () {
                var ques_id = req.body['id'];
                $.models.questionnaire.findOne({
                    where: {
                        id: parseInt(ques_id)
                    },
                    include: [{all: true}]
                }).then(function (ques) {

                    return $.models.questions.max('display_order', {where: {questionnaire_id: ques.id}});


                }).then(function (question_max) {

                    if (question_max) {
                        max_display_order = question_max + 1;
                    }

                    return $.models.questions.build({
                        questionnaire_id: parseInt(ques_id),
                        question: req.body['question'],
                        choices: JSON.stringify(choices),
                        answer: key_answer || null,
                        display_order: max_display_order
                    }).save();

                }).then(function (question) {
                    res.send({error: 0, message: 'quetion created', data: {question: question}});
                });
            }, function (errors) {
                res.send({error: 1, message: 'Please check fields', data: {errors: errors}});


            });


        }
    }],

    "update_question": ['POST', function (req, res) {
        $.models.questions.findOne({
            where: {
                id: parseInt(req.body['question_id'],10),
                questionnaire_id: parseInt(req.body['id'],10)
            },
            include: [{all: true}]
        }).then(function (ques) {
            var choices = req.body['choices'];
            var key_answer = _.findKey(choices, function (o) {
                return o.answer == 'true';
            });
            return ques.update({
                question: req.body['question'],
                choices: JSON.stringify(choices),
                answer: key_answer
            });

        }).then(function () {
            res.send({error: 0, message: 'deleted'});
        })
    }],

    "delete_question": ['POST', function (req, res) {
        $.models.questions.findOne({
            where: {
                id:parseInt(req.body['question_id'],10),
                questionnaire_id: parseInt(req.body['id'],10)
            },
            include: [{all: true}]
        }).then(function (ques) {
            return ques.destroy();

        }).then(function () {
            res.send({error: 0, message: 'deleted'});
        })
    }],


    "get_question": ['POST', function (req, res) {
        $.models.questions.findOne({
            where: {
                id:parseInt(req.body['question_id'],10),
                questionnaire_id: parseInt(req.body['id'],10)
            },
            include: [{all: true}]
        }).then(function (question) {
            // console.log(question);
            res.send({error: 0, message: 'question', data: question});
        })
    }],

    "get_questions_dt": ['POST', function (req, res) {
        var permitted = $.helpers.permission.access(req, res, [1, 2], true);

        if (permitted) {
            var q_id = req.body['questionnaire_id'] || 1;
            datatable($.models.questions, req.body, {
                where: {
                    questionnaire_id: parseInt(q_id)
                },
                include: [{all: true}]
            }).then(function (result) {
                res.send(result);
            });
        }


    }],


    'getRandomQuestions': ['GET', function (req, res) {
        var questionnaire_id = 1;

        var permitted = $.helpers.permission.access(req, res, [1, 2, 3], true);

        if (permitted) {
            $.models.questions.findAll({
                include: [{all: true}]
            }).then(function (question) {
                // console.log(req.session.user.id);
                var new_question = [];
                question.forEach(function (q) {
                    var user_key = _.findKey(q.answers, function (qq) {
                        return qq.user_id == req.session.user.id;
                    });

                    var question_result = {
                        id: q.id,
                        question: q.question,
                        answers: q.answers,
                        choices: q.choices,
                        display_order: q.display_order,
                        questionnaire_id: q.questionnaire_id
                    };

                    if (!user_key) {
                        new_question.push(question_result)
                    }
                });

                res.send({error: 0, message: 'question', data: new_question});
            });
        }
    }],


    'submit_answers': ['POST', function (req, res) {

        $.models.answers.findOne({
            where: {
                user_id: req.session.user.id,
                question_id: parseInt(req.body.id, 10)
            }
        }).then(function (answer) {
            if (answer) {
                answer.update({
                    answer: req.body.answer
                }).then(function () {
                    res.send({
                        error: 0,
                        message: 'submitted',
                        data: {answered: req.body.id}
                    });
                })
            } else {
                $.models.answers.build({
                    user_id: req.session.user.id,
                    question_id: parseInt(req.body.id, 10),
                    answer: req.body.answer
                }).save().then(function (anwer) {
                    res.send({
                        error: 0,
                        message: 'submitted',
                        data: {answered: req.body.id}
                    });
                })
            }

        });


    }],


    "get_scores": ['GET', function (req, res) {
        var permitted = $.helpers.permission.access(req, res, [1, 2, 3], true);

        if (permitted) {
            var questionnaire_id = parseInt(req.query['questionnaire_id'],10);
            $.models.questionnaire.findAll({
                where:{
                    id:questionnaire_id
                },
                include: [
                    {
                        model: $.models.questions,
                        include: [{
                            model: $.models.answers, include: [
                                {
                                    model: $.models.users,
                                    include:{
                                        model:$.models.persons
                                    }
                                }
                            ]
                        }]
                    }
                ]
            }).then(function (qq) {
                var questions = qq[0].questions;
                // var overall = questions.length;
                var user_answers = [];

                var user_answers_count = [];

                // questions.
                // console.log(questions);

                questions.forEach(function (question) {

                    question.answers.forEach(function (answer) {
                        var u_id = answer.user_id;
                        var key_answer = _.findKey(user_answers,function (o) {
                            return o.user_id == u_id;
                        });
                        if(!key_answer){
                            user_answers.push({
                                user_id:u_id,
                                first_name:answer.user.person.first_name,
                                last_name:answer.user.person.last_name,
                                score:(answer.answer == question.answer ? 1 : 0)
                            });

                        }else{
                            var el = user_answers[key_answer];
                            if(answer.answer == question.answer){
                                el.score = el.score + 1;
                            }


                        }
                    });

                });

                // console.log(user_answers);



                res.send(JSON.stringify(user_answers));


            })


        }


    }]



    /**
     * First parameter will be the request method GET, POST, PUT, DELETE
     * Second paramter will be your middleware specified in app/middleware/index.js
     * Order your middleware from left to right
     "index": ['GET',['my_custom_middleware'] ,function (req, res) {

        res.render('main');

    }, {title: 'My page title', name: 'slug_page_name'}],

     */
};