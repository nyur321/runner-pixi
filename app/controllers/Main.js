module.exports = {

    "index": ['GET', function (req, res) {

        res.render('main');

    }, {title: 'My Home Page', name: 'home'}],

    "about":['GET',function (req,res) {

        res.render('about');
    }]

    /**
     * First parameter will be the request method GET, POST, PUT, DELETE
     * Second paramter will be your middleware specified in app/middleware/index.js
     * Order your middleware from left to right
     "index": ['GET',['my_custom_middleware'] ,function (req, res) {

        res.render('main');

    }, {title: 'My page title', name: 'slug_page_name'}],

     */
};