module.exports = {

    "index": ['GET', function (req, res) {

        res.render('questionnaire');

    }, {title: 'My Home Page', name: 'home'}],


    "manage": ['GET', function (req, res) {
        var permitted = $.helpers.permission.access(req,res,[1,2],false);

        if(permitted){
            res.render('questionnaire');

        }



    }, {title: 'My Home Page', name: 'home'}],


    "create":['GET',function(req,res){
        var permitted = $.helpers.permission.access(req,res,[1,2],false);
        if(permitted){
            res.render('questionnaire/create');
        }

    },{title:'Create Questionnaire',name:'create_questionnaire'}],
    
    "edit":['GET',function (req,res) {
        var permitted = $.helpers.permission.access(req,res,[1,2],false);

        if(permitted){
            var ques_id = req.query['id'];
            $.models.questionnaire.findOne({
                where:{
                    id:ques_id
                },
                include:[{all:true}]
            }).then(function (ques) {
               res.render('questionnaire/edit',{questionnaire:ques});
            });

            
        }
    }],
    


};