

module.exports = {

    /**
     * This is the Default middleware all controllers and routes uses the default middleware
     * */

    'default': function (req, res, next) {
        $.helpers.session.reset(req,res);

        res.locals.menu = {
            'manage':[]
        };

        res.locals.user = {
            id:0,
            fname:'Anonymous',
            lname:'Account',
            role:'anonymous',
            role_id:0
        };


        if(req.session.user){

            res.locals.user = {
                id:req.session.user.id,
                fname:req.session.user.first_name,
                lname:req.session.user.last_name,
                role:req.session.user.role,
                role_id:req.session.user.role_id
            };



            res.locals.menu['manage'] = [{
                label:'Account',
                url:'/user/profile'
            }];

            //admin
            if(req.session.user.role_id == 1){
                res.locals.menu['manage'].push({
                    label: 'People',
                    url: '/user/manage'
                });
                res.locals.menu['manage'].push({
                    label:'Questionnaires',
                    url:'/questionnaire/manage'
                });
            }

            //instructor
            if(req.session.user.role_id == 2){
                res.locals.menu['manage'].push({
                    label:'Questionnaires',
                    url:'/questionnaire/manage'
                });
            }

            //student
            if(req.session.user.role_id == 3){

            }

        }


        res.locals.pageType = 'layout-top-nav';
        res.locals.skin = 'skin-blue';


        next();
    }


    /** You can add more middleware here*/



};