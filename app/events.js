"use strict";
var async = require('async');

module.exports = {

    onModelsLoad:function(){
        async.series([
            function (callback) {
                $.models.roles.bulkCreate([
                    {
                        id:1,role:'admin',description:'Administrator'
                    },
                    {
                        id:2,role:'instructor',description:'Instructor'
                    },
                    {
                        id:3,role:'student',description:'Student'
                    }
                ],{
                    updateOnDuplicate:true
                }).then(function () {
                    callback();


                });
            },
            function (callback) {
                $.models.persons.upsert({
                    id:1,
                    first_name:'Administrator',
                    last_name:'Account',
                }).then(function () {
                    callback();
                });
            },
            function (callback) {
                $.models.users.upsert({
                    id:1,
                    username:'admin',
                    email:'admin@gmail.com',
                    password:'admin',
                    validated:1,
                    role_id:1,
                    person_id:1,
                }).then(function () {



                    callback();


                });

            }
        ],function () {

        });


    },

    onControllersLoad:function(){

    },

    onRoutesLoad:function(){

    },

    onSocketLoad:function(){

    },

    onPluginsLoad:function(){

    },

    onServerLoad:function(){

    }


}