//Detect compatibility
(function ($, PIXI) {

    $(document).ready(function () {
        var questions = [],current_question = 0,answered = [];
        if($('#game-container').length > 0){
            updateQuestions(function () {
                init();
            });
        }

        function updateQuestions(callback) {
            $.ajax({
                url:'/api/getRandomQuestions',
                method:'get',
                dataType:'json',
                success:function (questions_data) {
                    questions = questions_data.data;
                    callback(questions);
                }
            });
        }

        function init() {
            var type = "WebGL";
            if (!PIXI.utils.isWebGLSupported()) {
                type = "canvas";
            }
            PIXI.utils.sayHello(type);

            var keymaps = {
                'pause': 32,
                'test_question': 32,
                'jump': 38,
                'crouch': 40,
            };
            //create renderer

            var app = new PIXI.Application(700, 512, {backgroundColor: 0x1099bb});
            var ctrl_pause, ctrl_jump, ctrl_crouch;
            var reload_page = keyboard(116);
            reload_page.press = function () {
                window.location.reload();
            };
            function updateKeymaps(maps) {
                ctrl_pause = keyboard(maps['pause']);
                ctrl_jump = keyboard(maps['jump']);
                ctrl_crouch = keyboard(maps['crouch']);
            }

            updateKeymaps(keymaps);


            //Add the canvas to the HTML document

            $('#game-container').append(app.view);


            //Create a container object called the `stage`
            var tux,
                cat,
                pack,
                started = false,
                game_speed = 4,
                state = 0,
                high_score = 0,
                floor_container,
                score = 0,
                elapsed_time = 0,
                score_board,
                floor,
                pauseMenu,
                tilingtop,
                tilingbottom,
                currentTux,
                jump_ctr = 180,
                jump_available = true,
                jumping = false,
                jump_height_relative = 120,
                gravity = 2,
                jumpVelocity = 10,
                jump_up_mul = 1,
                jump_down_mul = 1,
                spawn_distance = 500,
                spawn_interval = [1000,5000],
                spawned_ghosts = [],
                reached = false,
                menu_container,
                menu_start_btn;
            PIXI.loader
                .add([
                    //{name: 'tux', url: "public/img/tux.png"},
                    //{name: 'cat', url: "public/img/cat.png"},
                    {name: 'resume_btn', url: "public/img/resume_btn.png"},
                    {name: 'pack', url: "public/img/tppack.json"},
                    {name: 'tux', url: "public/img/tux.json"},
                    {name: 'floor', url: "public/img/floor.json"},
                ])
                .on("progress", loadProgressHandler)
                .load(assetLoaded);


            function loadProgressHandler(loader, resource) {
                console.log("progress: " + loader.progress + "%");
            }

            function assetLoaded() {

                tux = PIXI.loader.resources["tux"].textures;
                pack = PIXI.loader.resources["pack"].textures;

                //loadFloor();

                pauseMenu = loadPauseMenu();
                pauseMenu.visible = true;

                app.stage.addChild(loadMenu());
                app.stage.addChild(loadFloor(app.renderer.width));
            }

            function loadPauseMenu() {
                var pause_menu_container = new PIXI.Container();
                pause_menu_container.width = app.renderer.width;
                pause_menu_container.height = app.renderer.height;

                pause_menu_container.x = 0;
                pause_menu_container.y = 0;

                var menu_resume_btn = new PIXI.Sprite(PIXI.loader.resources["resume_btn"].textures);
                menu_resume_btn.interactive = true;
                menu_resume_btn.buttonMode = true;

                menu_resume_btn.x = (app.renderer.width - menu_resume_btn.width) / 2;
                menu_resume_btn.y = (app.renderer.width - menu_resume_btn.width) / 2;

                menu_resume_btn.on('pointerdown', resume);

                pause_menu_container.addChild(menu_resume_btn);
                return menu_resume_btn;

            }


            function actionTux(action) {


            }
            
            
            function executeEvery() {
                console.log('executing every 1k frames');
            }

            function loadFloor(width) {
                floor_container = new PIXI.Container();
                var floor_texture = PIXI.loader.resources["floor"].textures;
                var floor_top = floor_texture['top'];
                var floor_bottom = floor_texture['bottom'];

                tilingtop = new PIXI.extras.TilingSprite(
                    floor_top,
                    999999,
                    32
                );
                tilingbottom = new PIXI.extras.TilingSprite(
                    floor_bottom,
                    999999,
                    200
                );

                tilingbottom.x = 0;
                tilingbottom.y = 32;

                tilingtop.x = 0;
                tilingtop.y = 0;

                floor_container.width = width;
                floor_container.height = 300;
                floor_container.y = 350;


                floor_container.addChild(tilingtop);
                floor_container.addChild(tilingbottom);
                return floor_container;
            }


            function moveFloor(x) {
                tilingtop.x = x;
                tilingbottom.x = x;
            }


            function loadMenu() {
                menu_container = new PIXI.Container();
                menu_container.width = app.renderer.width;
                menu_container.height = app.renderer.height;

                menu_container.backgroundColor = 0x061639;
                menu_start_btn = new PIXI.Sprite(pack['button-new.png']);
                menu_start_btn.interactive = true;
                menu_start_btn.buttonMode = true;

                menu_start_btn.x = (app.renderer.width - menu_start_btn.width) / 2;
                menu_start_btn.y = (app.renderer.width - menu_start_btn.width) / 2;


                menu_start_btn.on('pointerdown', function () {
                    console.log('game started');
                    started = true;
                    menu_container.visible = false;
                    startgame();
                });

                var text = new PIXI.Text('Play game', {fontFamily: 'Arial', fontSize: 24, fill: 0xff1010, align: 'center'});
                menu_container.addChild(menu_start_btn);

                menu_container.x = 0;
                menu_container.y = 0;

                //menu_container.x = (app.renderer.width - menu_container.width) / 2;
                //menu_container.y = (app.renderer.height - menu_container.height) / 2;

                return menu_container;
            }

            function startgame() {
                state = 1;
                score = 0;
                started = true;
                var frames = 0;
                var lastTime = 0;
                var timeSinceLastFrame = 0;
                score_board = new PIXI.Text('Score: 0', {
                    fontFamily: 'Arial',
                    fontSize: 20,
                    fill: 0xff1010,
                    align: 'center'
                });
                score_board = new PIXI.Text('Time: 00:00', {
                    fontFamily: 'Arial',
                    fontSize: 20,
                    fill: 0xff1010,
                    align: 'center'
                });
                score_board.x = widthPercentage(80);
                score_board.y = 10;
                app.stage.addChild(score_board);

                gameStarted();

                app.ticker.add(function (delta) {
                    score_board.text = 'Score: ' + score;
                    animate_tux(delta);
                    moveFloor((frames * game_speed) * -1);
                    lastTime = app.ticker.lastTime / 1000;


                    $('body canvas').attr('delta', delta);
                    $('body canvas').attr('data-fps', app.ticker.FPS);
                    if (parseInt(app.ticker.lastTime, 10) % 2 != 0) {
                        score++;
                    }

                    gameOnGoing(frames);

                    if(spawned_ghosts.length > 0){
                        spawned_ghosts.forEach(function(ghost){
                            ghost.visible = true;
                            ghost.vx = 5;
                            ghost.x -= game_speed;
                            var ghosthit = hitTestRectangle(currentTux, ghost);
                            if(ghosthit){
                                gameOver();
                            }



                        });
                    }



                    //console.log(parseInt());
                    //if(delta == 1.0){
                    //    console.log(elapsed_time);
                    //    elapsed_time++;
                    //}
                    frames++

                });
            }

            function animate_tux() {
                if (state) {


                }

            }

            function gameStarted() {
                currentTux = new PIXI.Sprite(tux['standing-1']);
                currentTux.x = 0;
                // currentTux.y = 289;
                currentTux.y = 100;


                app.stage.addChild(currentTux);
            }

            function selectQuestion() {

                if(questions.length >= current_question){
                    var question = questions[current_question];
                    if(question){
                        $('#question-modal').find('.question').html(question.question);

                        var choices = JSON.parse(question.choices);
                        var choice_txt = '<select class="form-control" data-id="'+question.id+'" name="choice">';
                        for(var choices_key in choices){
                            choice_txt += '<option value="'+choices_key+'">'+choices[choices_key].value+'</option>';
                        }
                        choice_txt += '</select>';
                        $('#question-modal').find('.question-choices').html(choice_txt);
                        resume();
                        $('#question-modal').modal('show');
                    }

                    // console.log(questions[current_question]);

                }



            }


            $('#submit-answer').on('submit',function (e) {
                e.preventDefault();
                var form = $('#submit-answer');
                var select_box = form.find('select[name="choice"]');
                $.ajax({
                    url:'/api/submit_answers',
                    method:'post',
                    data:{
                        id:form.find('select[name="choice"]').attr('data-id'),
                        answer:select_box.val()
                    },
                    dataType:'json',
                    success:function (response) {
                        var answer = response.data.answered;
                        answered.push(answer);
                        current_question++;

                        updateQuestions(function () {
                            $('#question-modal').modal('hide');
                            resume();
                        });

                    }
                })


            });



            function gameOnGoing(frames) {
                var status = '';
                currentTux.vx = 2;
                currentTux.vy = jumpVelocity;

                var onground = hitTestRectangle(currentTux, floor_container);
                if (currentTux.x < 40) {
                    currentTux.x += currentTux.vx;
                }
                //Jumping algorithm
                if (jumping) {
                    var jmp_target = jump_height_relative;
                    status = 'jumping';
                    if(currentTux.y <= jmp_target){
                        reached = true;
                    }
                    if (currentTux.y > jmp_target && !reached) {
                        currentTux.y -= (currentTux.vy * jump_up_mul);
                    }else{
                        if(!onground){
                            currentTux.y += (currentTux.vy * jump_down_mul);
                        }else{
                            //reached = false;
                        }
                    }
                } else {
                    if (!onground) {
                        jump_available = false;
                        status = 'on air';
                        currentTux.y += currentTux.vy;
                    } else {
                        status = 'on ground';
                        jump_available = true;

                    }
                }

                if (frames > 0 && frames % 1000 === 0) {
                    // console.log("Divisible by 1000");
                    selectQuestion();

                }


                //Spawn ghost

                //console.log(Math.floor(Math.random() * (500 - 100 + 1)));
                //console.log('real '+app.ticker.lastTime);
                //console.log('tick '+(frames/500)%1==0);

                if((frames/50)%1==0){
                    var whatdo = probability([['spawn',0.3],['nothing',0.7]]);

                    if(whatdo == 'spawn'){
                        var ghost = new PIXI.Sprite(pack['blob.png']);
                        var interval_spawn = app.renderer.width - randomInt(-200,-10);
                        ghost.y = 320;
                        ghost.x = interval_spawn;
                        //ghost.visible = false;
                        spawned_ghosts.push(ghost);
                        app.stage.addChild(ghost);


                    }
                }











                //console.log(randomInt(floor_container.x + 1000,floor_container.x + 7000));






            }

            function gameOver() {
                app.stop();
                state = 0;
                started = false;

                menu_container.visible = true;
                menu_start_btn.visible = true;
                menu_container.zIndex = 9999;
                menu_start_btn.zIndex = 9999;
                app.stage.addChild(menu_container);


            }

            ctrl_jump.press = function () {
                if (jump_available) {
                    console.log('jump pressed');
                    reached = false;
                    jumping = true;
                }
                jump_ctr = 0;
                // console.log('jump pressed');
            };

            ctrl_pause.press = resume;

            function resume() {
                if (started) {
                    if (state == 1) {
                        app.stop();
                        state = 0;
                        console.log('game paused');
                        pauseMenu.visible = true;
                    } else {
                        app.start();
                        state = 1;
                        pauseMenu.visible = false;
                        console.log('game resumed');
                    }
                }
            }


            function randomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }

            function chance(c){

                var ch = Math.random() * 100;
                if(ch >= c){
                    return true;
                }else{
                    return false;
                }

            }

            function probability(p){

                var weights = []; // probabilities
                var results = []; // values to return


                for(var k in p){
                    if(p[k].length == 2){
                        results.push(p[k][0]);
                        if(p[k][1] <= 1.0){
                            weights.push(p[k][1]);
                        }else{
                            if(p[k][1] <= 100){
                                var w = p[k][1] / 100;
                                weights.push(w);
                            }
                        }
                    }
                }


                var num = Math.random(),
                    s = 0,
                    lastIndex = weights.length - 1;


                for (var i = 0; i < lastIndex; ++i) {
                    s += weights[i];
                    if (num < s) {
                        return results[i];
                    }
                }
                return results[lastIndex];

            }

            function widthPercentage(percent) {
                return app.renderer.width * (parseInt(percent, 10) / 100);
            }


            function keyboard(keyCode) {
                var key = {};
                key.code = keyCode;
                key.isDown = false;
                key.isUp = true;
                key.press = undefined;
                key.release = undefined;
                //The `downHandler`
                key.downHandler = function (event) {
                    if (event.keyCode === key.code) {
                        if (key.isUp && key.press) key.press();
                        key.isDown = true;
                        key.isUp = false;
                    }
                    event.preventDefault();
                };

                //The `upHandler`
                key.upHandler = function (event) {
                    if (event.keyCode === key.code) {
                        if (key.isDown && key.release) key.release();
                        key.isDown = false;
                        key.isUp = true;
                    }
                    event.preventDefault();
                };

                //Attach event listeners
                window.addEventListener(
                    "keydown", key.downHandler.bind(key), false
                );
                window.addEventListener(
                    "keyup", key.upHandler.bind(key), false
                );
                return key;
            }

            function hitTestRectangle(r1, r2) {

                //Define the variables we'll need to calculate
                var hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

                //hit will determine whether there's a collision
                hit = false;

                //Find the center points of each sprite
                r1.centerX = r1.x + r1.width / 2;
                r1.centerY = r1.y + r1.height / 2;
                r2.centerX = r2.x + r2.width / 2;
                r2.centerY = r2.y + r2.height / 2;

                //Find the half-widths and half-heights of each sprite
                r1.halfWidth = r1.width / 2;
                r1.halfHeight = r1.height / 2;
                r2.halfWidth = r2.width / 2;
                r2.halfHeight = r2.height / 2;

                //Calculate the distance vector between the sprites
                vx = r1.centerX - r2.centerX;
                vy = r1.centerY - r2.centerY;

                //Figure out the combined half-widths and half-heights
                combinedHalfWidths = r1.halfWidth + r2.halfWidth;
                combinedHalfHeights = r1.halfHeight + r2.halfHeight;

                //Check for a collision on the x axis
                if (Math.abs(vx) < combinedHalfWidths) {

                    //A collision might be occuring. Check for a collision on the y axis
                    if (Math.abs(vy) < combinedHalfHeights) {

                        //There's definitely a collision happening
                        hit = true;
                    } else {

                        //There's no collision on the y axis
                        hit = false;
                    }
                } else {

                    //There's no collision on the x axis
                    hit = false;
                }

                //`hit` will be either `true` or `false`
                return hit;
            };

        
        }

    });


})(jQuery, PIXI);
