$(document).ready(function () {

    $('#register-user-form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var original_submit = form.find('button').html();
        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            dataType: 'json',
            data: form.serialize(),
            beforeSend: function () {
                form.find('button').html('Submitting');
            },
            success: function (res) {
                form.find('span.error').html('');
                form.find('.field-error').removeClass('field-error');
                form.find('button').html(original_submit);
                $(form.attr('data-message')).html('');
                if (res.error) {
                    $(form.attr('data-message')).html(res.message);
                    if ($.isArray(res.data.errors)) {
                        $.each(res.data.errors, function (i, v) {
                            var field = form.find('[name="' + v.param + '"]');
                            field.addClass('field-error');
                            field.siblings('span.error').append(v.msg + '<br>');

                        });
                    }
                } else {

                    form.find('button').html('Redirecting');
                    window.location = '/';

                }

            }
        });
    });


    $('.ajax').on('click', function (e) {
        e.preventDefault();
        var link = $(this);
        var form = $(link.attr('target-form'));
        console.log(form.serialize())
        var original_submit = link.html();
        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            dataType: 'json',
            data: form.serialize(),
            beforeSend: function () {
                link.html('Logging in');
            },
            success: function (res) {
                console.log(res);
                form.find('span.error').html('');
                form.find('.field-error').removeClass('field-error');
                link.html(original_submit);
                $(form.attr('data-message')).html('');
                if (res.error) {
                    $(form.attr('data-message')).html(res.message);
                    if(res.hasOwnProperty('data')){
                        if(res.data.hasOwnProperty('errors')){
                            if ($.isArray(res.data.errors)) {
                                $.each(res.data.errors, function (i, v) {
                                    var field = form.find('[name="' + v.param + '"]');
                                    field.addClass('field-error');
                                    field.siblings('span.error').append(v.msg + '<br>');

                                });
                            }
                        }
                    }

                    if(res.hasOwnProperty('code')){
                        if(res.code == 'user_deleted'){
                            alert('This user has been deleted')
                        }
                    }

                } else {

                    link.html('Redirecting');
                    window.location = '/';

                }

            }
        });


    });


    if ($('#data-quest').length > 0) {
        $('#data-quest').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/api/get_questionnaires",
                "type": "POST"
            },
            "columns": [
                {"data": "title"},
                {"data": "description"},
                {
                    "data": "user",
                    "render": function (data, type, full, meta) {
                        if(data.is_deleted == 1){
                            return '<span class="text-danger">'+data.username+' (user deleted)</span>';
                        }
                        return data.username;
                    }
                },
                {
                    "data": null,
                    "render": function (data, type, full, meta) {
                        console.log(data)
                        return '<a href="/questionnaire/edit?id=' + data.id + '" class="btn btn-flat btn-success" >open</a>'
                    }
                }
            ]
        });
    }

    var data_users;
    if ($('#data-users').length > 0) {
        data_users = $('#data-users').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/api/get_users",
                "type": "POST"
            },
            "columns": [
                {"data": "username"},
                {"data": "email"},
                {
                    "data": "person",
                    "render": function (data, type, full, meta) {
                        // console.log(data.username)
                        return data.first_name;
                    }
                },
                {
                    "data": "person",
                    "render": function (data, type, full, meta) {
                        // console.log(data.username)
                        return data.last_name;
                    }
                },
                {
                    "data": null,
                    "render": function (data, type, full, meta) {

                        return data.role.role;
                    }
                },
                {
                    "data": null,
                    "render": function (data, type, full, meta) {
                        // console.log(data)
                        var str = '<a href="#" class="btn btn-flat btn-success user-control" data-id="' + data.id + '" action="view">View/Edit</a>';
                        str += '<a href="#" class="btn btn-flat btn-success user-control" data-id="' + data.id + '" action="delete">Delete</a>';


                        return str;
                    }
                }
            ]
        });
    }


    $('.user-menu .dropdown-menu').on({
        "click": function (e) {
            e.stopPropagation();
        }
    });


    if ($('#create-questionnaire-form').length > 0) {
        $('#create-questionnaire-form').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            var original_submit = form.find('button').html();
            $.ajax({
                url: form.attr('action'),
                method: 'POST',
                dataType: 'json',
                data: form.serialize(),
                beforeSend: function () {
                    form.find('button').html('Submitting');
                },
                success: function (res) {
                    form.find('span.error').html('');
                    form.find('.field-error').removeClass('field-error');
                    form.find('button').html(original_submit);
                    $(form.attr('data-message')).html('');
                    if (res.error) {
                        $(form.attr('data-message')).html(res.message);
                        if ($.isArray(res.data.errors)) {
                            $.each(res.data.errors, function (i, v) {
                                var field = form.find('[name="' + v.param + '"]');
                                field.addClass('field-error');
                                field.siblings('span.error').append(v.msg + '<br>');

                            });
                        }
                    } else {
                        console.log(res.data);
                        form.find('button').html('Redirecting');
                        window.location = res.data.redirect;

                    }

                }
            });

        })


    }


    $('#data-users,.box-create').on('click', '.user-control', function (e) {
        e.preventDefault();
        var btn = $(this), modal = $('#create-edit-user');
        var dat_id = btn.attr('data-id');
        switch (btn.attr('action')) {
            case 'create':
                modal.find('input,textarea').each(function (i, v) {
                    $(v).val('');
                });
                modal.find('form').attr('action', '/api/create_user');
                modal.find('input[name="update-id"]').val('');
                modal.modal('show');

                break;
            case 'view':
                $.ajax({
                    url: '/api/get_user',
                    method: 'post',
                    data: {id: dat_id},
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        modal.find('input,textarea').each(function (i, v) {
                            $(v).val('');
                        });

                        modal.find('form').attr('action', '/api/update_user')
                        modal.find('input[name="update-id"]').val(data.id);
                        modal.find('select[name="role-id"]').val(data.role_id);
                        modal.find('input[name="email"]').val(data.email);
                        modal.find('input[name="username"]').val(data.username);
                        modal.find('input[name="first-name"]').val(data.person.first_name);
                        modal.find('input[name="last-name"]').val(data.person.last_name);
                        modal.modal('show');
                    }

                });
                break;

            case 'delete':
                var retVal = confirm("Are you sure you want to delete this user?");
                if (retVal == true) {
                    $.ajax({
                        url: '/api/delete_user',
                        method: 'POST',
                        data: {
                            id: dat_id
                        },
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);

                            data_users.ajax.reload();
                        }
                    })

                }
                else {


                }


                break;


        }


    })

    if ($('#update-user-profile').length > 0) {
        $('#update-user-profile').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            var original_submit = form.find('button').html();
            $.ajax({
                url: form.attr('action'),
                method: 'POST',
                dataType: 'json',
                data: form.serialize(),
                beforeSend: function () {
                    form.find('button').html('Submitting');
                },
                success: function (res) {
                    form.find('span.error').html('');
                    form.find('.field-error').removeClass('field-error');
                    form.find('button').html(original_submit);
                    $(form.attr('data-message')).html('');
                    if (res.error) {
                        $(form.attr('data-message')).html(res.message);
                        if ($.isArray(res.data.errors)) {
                            $.each(res.data.errors, function (i, v) {
                                var field = form.find('[name="' + v.param + '"]');
                                field.addClass('field-error');
                                field.siblings('span.error').append(v.msg + '<br>');

                            });
                        }
                    } else {
                        form.find(form.attr('data-message')).html(res.message);
                        form.find('button').html(original_submit);
                        // window.location = '/';

                    }

                }
            });
        });
    }


    if ($('#create-edit-form').length > 0) {
        $('#create-edit-form').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            var original_submit = form.find('button[type="submit"]').html();
            console.log(form.attr('action'));
            $.ajax({
                url: form.attr('action'),
                method: 'POST',
                dataType: 'json',
                data: form.serialize(),
                beforeSend: function () {
                    form.find('button').html('Submitting');
                },
                success: function (res) {
                    console.log(res)
                    form.find('span.error').html('');
                    form.find('.field-error').removeClass('field-error');
                    form.find('button[type="submit"]').html(original_submit);
                    $(form.attr('data-message')).html('');
                    if (res.error) {
                        $(form.attr('data-message')).html(res.message);
                        if ($.isArray(res.data.errors)) {
                            $.each(res.data.errors, function (i, v) {
                                var field = form.find('[name="' + v.param + '"]');
                                field.addClass('field-error');
                                field.siblings('span.error').append(v.msg + '<br>');

                            });
                        }
                    } else {
                        form.find(form.attr('data-message')).html(res.message);
                        // window.location = '/';
                        $('#create-edit-user').modal('hide');
                        data_users.ajax.reload();

                    }

                }
            });
        });
    }


    var questions_datatable;
    if ($('#questionnaire-edit-page').length > 0) {
        // updateQuestions();

        questions_datatable = $('#questions').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/api/get_questions_dt",
                "type": "POST",
                "data": function (d) {
                    d.questionnaire_id = $('#add-submit-question').attr('questionnaire-id')
                }
            },
            "buttons": [
                {
                    text: 'Add Questions',
                    action: function (e, dt, node, config) {
                        alert('Button activated');
                    }
                }
            ],
            "columns": [
                {"data": "question"},
                {
                    "data": null,
                    "render": function (data, type, full, meta) {
                        var str = '<a href="#" class="btn btn-flat btn-success question-control" data-id="' + data.id + '" action="view">View/Edit</a>';
                        str += '<a href="#" class="btn btn-flat btn-success question-control" data-id="' + data.id + '" action="delete">Delete</a>';


                        return str;
                    }
                }
            ]
        });


        questions_datatable.buttons().container()
            .appendTo('#questions .col-sm-6:eq(0)');

        var modal_quetion = $('#create-question');
        var modal_action = '';
        bindCheckboxGroup();
        var current_question_id;

        $('#questionnaire-edit-page').on('click', '.question-control', function (e) {
            $('#question-input').val();
            console.log('quetion btn triggered');
            e.preventDefault();
            var this_btn = $(this);
            modal_action = this_btn.attr('action');
            switch (this_btn.attr('action')) {
                case 'create':
                    modal_quetion.find('.modal-title').html('Add question');
                    $('.checkbox-container').html('');
                    $('#add-submit-question').html('Update');
                    var str = '';
                    for (var i = 0; i < 4; i++) {
                        str += '<label class="checkbox-inline">';
                        str += '<input type="checkbox" class="" name="answer" value="" placeholder="" data-single="true">';
                        str += '<input type="text" class="form-control" name="choice-input" data-answer="" value="" placeholder="">';
                        str += '</label>';

                    }

                    modal_quetion.find('#question-input').html('');
                    modal_quetion.find('.modal-title').html('Add question');
                    $('#add-submit-question').html('Add');
                    $('.checkbox-container').html(str);
                    bindCheckboxGroup();
                    modal_quetion.modal('show');

                    break;

                case 'view':

                    $.ajax({
                        url: '/api/get_question',
                        method: 'POST',
                        data: {
                            id: $('#questionnaire-edit-page').attr('questionnaire-id'),
                            question_id: this_btn.attr('data-id')
                        },
                        success: function (response) {
                            console.log(response);
                            current_question_id = response.data.id;
                            modal_quetion.find('#question-input').html(response.data.question);
                            var choices_arr = JSON.parse(response.data.choices);
                            console.log(choices_arr);
                            var str = '';
                            choices_arr.forEach(function (choice) {
                                str += '<label class="checkbox-inline">';
                                var is_checked = '';
                                if (choice.answer == 'true') {
                                    is_checked = 'checked';
                                }
                                str += '<input type="checkbox" class="" name="answer" ' + is_checked + ' value="" placeholder="" data-single="true">';
                                str += '<input type="text" class="form-control" name="choice-input" data-answer="" value="' + choice.value + '" placeholder="">';
                                str += '</label>';

                            });

                            modal_quetion.find('.modal-title').html('Update question');
                            $('.checkbox-container').html(str);
                            $('#add-submit-question').html('Update');
                            bindCheckboxGroup();
                            modal_quetion.modal('show');

                        }
                    });

                    break;

                case 'delete':
                    $.ajax({
                        url: '/api/delete_question',
                        method: 'POST',
                        data: {
                            id: $('#questionnaire-edit-page').attr('questionnaire-id'),
                            question_id: this_btn.attr('data-id')
                        },
                        success: function (response) {
                            console.log(response);
                            questions_datatable.ajax.reload();

                        }
                    });
                    break;

            }


        });

        $('#add-submit-question').on('click', function (e) {
            e.preventDefault();
            var data_choices = [];
            modal_quetion.find('.checkbox-container input[type="checkbox"]').each(function (i, v) {
                if ($(v).is(':checked')) {
                    console.log($(v).siblings('input[type="text"]').attr('data-answer', 'true'))
                }
            });
            modal_quetion.find('.checkbox-container input[type="text"]').each(function (i, v) {
                var data_choice = {
                    answer: false
                };
                if ($(v).attr('data-answer') == 'true') {
                    data_choice['answer'] = true;
                }
                if ($(v).val() != '') {
                    data_choice['value'] = $(v).val();
                    data_choices.push(data_choice)
                }


            });

            var send_data = {
                id: $('#add-submit-question').attr('questionnaire-id'),
                question: $('#question-input').val(),
                choices: data_choices
            };
            var s_url = 'add_question';
            switch (modal_action) {
                case 'create':
                    s_url = 'add_question';
                    break;
                case 'view':
                    send_data['question_id'] = current_question_id;
                    s_url = 'update_question';
                    break;
            }

            console.log(modal_action);
            console.log(send_data);


            $.ajax({
                url: '/api/'+s_url,
                method: 'POST',
                data: send_data,
                success: function (data) {
                    questions_datatable.ajax.reload();
                    modal_quetion.modal('hide');

                }
            });


        })


        $('#add-choices').on('click', function (e) {
            $('.checkbox-container').append($('.choice-template-append').html());
            bindCheckboxGroup();
        });


        updateScores($('#questionnaire-scores'));


    }

    function updateScores(score_table) {
        console.log(score_table);
        $.ajax({
            url:"/api/get_scores?questionnaire_id="+score_table.attr('questionnaire-id'),
            dataType:'json',
            success:function (data) {
                console.log(data);
                var str = '';
                $.each(data,function (index,dat) {
                    str += '<tr>';
                    str += '<td>';
                    str += dat.first_name+' '+dat.last_name;
                    str += '</td>';
                    str += '<td>';
                    str += dat.score;
                    str += '</td>';
                });
                score_table.find('tbody').html(str)


            }
        })



    }

    function bindCheckboxGroup() {
        $(".checkbox-group input[type=checkbox]").on('click', function () {
            var $box = $(this);
            if ($(this).data('single')) {
                var name = $(this).attr('name');
                var others_text = $("input[for='" + $box.attr("id") + "']");

                if ($box.is(":checked")) {
                    var group = "input:checkbox[name='" + $box.attr("name") + "']";
                    if (others_text.data('check') == $box.attr('name')) {
                        if (others_text.length > 0) {
                            others_text.attr('disabled', false);
                        } else {
                            others_text.attr('disabled', true);
                        }
                    } else {
                        $("input[data-check='" + $box.attr("name") + "']").attr('disabled', true);
                    }
                    $(group).prop("checked", false);
                    $box.prop("checked", true);

                } else {
                    $box.prop("checked", false);
                    others_text.attr('disabled', true);
                    $("input[data-check='" + $box.attr("name") + "']").attr('disabled', true);
                }
            } else {
                if ($box.is(":checked")) {
                    $("input[data-check='" + $box.attr("name") + "']").attr('disabled', false);
                } else {
                    $("input[data-check='" + $box.attr("name") + "']").attr('disabled', true);
                }
            }

        });
    }


});
