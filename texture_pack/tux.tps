<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.3.3</string>
        <key>fileName</key>
        <string>C:/Users/nyur/Desktop/runner-pixi/texture_pack/tux.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>pixijs</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../static/public/img/tux.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../static/public/img/tux/close-eyes-1.png</key>
            <key type="filename">../static/public/img/tux/close-eyes-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.578947,0.571429</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,16,19,32</rect>
                <key>scale9Paddings</key>
                <rect>10,16,19,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/close-eyes-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.564103,0.596774</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,16,20,31</rect>
                <key>scale9Paddings</key>
                <rect>10,16,20,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/jump-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.6,0.590164</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,15,23,31</rect>
                <key>scale9Paddings</key>
                <rect>11,15,23,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/jump-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.619048,0.587302</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,16,21,32</rect>
                <key>scale9Paddings</key>
                <rect>11,16,21,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/jump-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.510204,0.590164</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,15,25,31</rect>
                <key>scale9Paddings</key>
                <rect>12,15,25,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/kick-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.585366,0.580645</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,16,21,31</rect>
                <key>scale9Paddings</key>
                <rect>10,16,21,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/kick-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.568182,0.580645</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,16,22,31</rect>
                <key>scale9Paddings</key>
                <rect>11,16,22,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/kick-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.553191,0.580645</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,16,24,31</rect>
                <key>scale9Paddings</key>
                <rect>12,16,24,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/lean-toward-1.png</key>
            <key type="filename">../static/public/img/tux/lean-toward-2.png</key>
            <key type="filename">../static/public/img/tux/lean-toward-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.456522,0.596774</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,16,23,31</rect>
                <key>scale9Paddings</key>
                <rect>12,16,23,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/standing-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.578947,0.596774</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,16,19,31</rect>
                <key>scale9Paddings</key>
                <rect>10,16,19,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/standing-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.578947,0.590164</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,15,19,31</rect>
                <key>scale9Paddings</key>
                <rect>10,15,19,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/standing-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.578947,0.606557</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,15,19,31</rect>
                <key>scale9Paddings</key>
                <rect>10,15,19,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/stun-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.477273,0.678571</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,14,22,28</rect>
                <key>scale9Paddings</key>
                <rect>11,14,22,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/stun-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.488372,0.660714</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,14,22,28</rect>
                <key>scale9Paddings</key>
                <rect>11,14,22,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/stun-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.525,0.660714</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,14,20,28</rect>
                <key>scale9Paddings</key>
                <rect>10,14,20,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/wave-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.609756,0.612903</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,16,21,31</rect>
                <key>scale9Paddings</key>
                <rect>10,16,21,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/wave-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.526316,0.596774</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,16,19,31</rect>
                <key>scale9Paddings</key>
                <rect>10,16,19,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../static/public/img/tux/wave-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.596774</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,16,19,31</rect>
                <key>scale9Paddings</key>
                <rect>10,16,19,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../static/public/img/tux/close-eyes-1.png</filename>
            <filename>../static/public/img/tux/close-eyes-2.png</filename>
            <filename>../static/public/img/tux/close-eyes-3.png</filename>
            <filename>../static/public/img/tux/jump-1.png</filename>
            <filename>../static/public/img/tux/jump-2.png</filename>
            <filename>../static/public/img/tux/jump-3.png</filename>
            <filename>../static/public/img/tux/kick-1.png</filename>
            <filename>../static/public/img/tux/kick-2.png</filename>
            <filename>../static/public/img/tux/kick-3.png</filename>
            <filename>../static/public/img/tux/lean-toward-1.png</filename>
            <filename>../static/public/img/tux/lean-toward-2.png</filename>
            <filename>../static/public/img/tux/lean-toward-3.png</filename>
            <filename>../static/public/img/tux/standing-1.png</filename>
            <filename>../static/public/img/tux/standing-2.png</filename>
            <filename>../static/public/img/tux/standing-3.png</filename>
            <filename>../static/public/img/tux/stun-1.png</filename>
            <filename>../static/public/img/tux/stun-2.png</filename>
            <filename>../static/public/img/tux/stun-3.png</filename>
            <filename>../static/public/img/tux/wave-1.png</filename>
            <filename>../static/public/img/tux/wave-2.png</filename>
            <filename>../static/public/img/tux/wave-3.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
