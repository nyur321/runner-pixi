/**
 * Creating shiftjs instance
 * */
var shinjsInstance = require('shinjs')();

if (!module.parent) {
    /**
     * starting shinjs from Node
     * */
    /**
     * globalize
     * */
    global.$ = shinjsInstance.shared();
    shinjsInstance.start(null,function(){
        //Update server instance io and site url
        //Update post process
        $ = shinjsInstance.shared();
    });
} else {
    /**
     * starting shinjs from Shinjs start command
     * */
    module.exports = shinjsInstance;
}

